#pragma once

#include <JavaScriptCore/JSContext.h>
#include <JavaScriptCore/JSObjectRef.h>

static JSObjectRef document_constructor(JSContextRef ctx,
                                        JSObjectRef constructor, size_t argc,
                                        const JSValueRef args[],
                                        JSValueRef *exception) {
  return JSObjectMake(ctx, nullptr, nullptr);
};

inline void add_document_class(JSContextRef context) {
  JSObjectRef global_object = JSContextGetGlobalObject(context);

  JSClassDefinition document_class_definition = kJSClassDefinitionEmpty;
  document_class_definition.className = "Document";

  JSClassRef document_class = JSClassCreate(&document_class_definition);

  JSObjectRef document_constructor_object =
      JSObjectMakeConstructor(context, document_class, document_constructor);

  JSObjectSetProperty(
      context, global_object, JSStringCreateWithUTF8CString("Document"),
      document_constructor_object, kJSPropertyAttributeNone, nullptr);
}
