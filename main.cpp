#include "runtime/dom.h"
#include <JavaScriptCore/JSContext.h>
#include <JavaScriptCore/JSExport.h>
#include <JavaScriptCore/JSManagedValue.h>
#include <JavaScriptCore/JSValue.h>
#include <JavaScriptCore/JSVirtualMachine.h>
#include <cstddef>
#include <cstdio>
#include <fstream>
#include <lexbor/dom/dom.h>
#include <sstream>

JSValueRef evaluate_script(JSContextRef context, JSStringRef script,
                           JSObjectRef global_object) {
  JSValueRef exception = nullptr;
  JSValueRef result =
      JSEvaluateScript(context, script, global_object, nullptr, 0, &exception);
  if (exception) {
    JSStringRef error_message = JSValueToStringCopy(context, exception, NULL);

    size_t js_size = JSStringGetMaximumUTF8CStringSize(error_message);
    char *js_buffer = (char *)malloc(js_size);
    JSStringGetUTF8CString(error_message, js_buffer, js_size);
    JSStringRelease(error_message);

    fprintf(stderr, "%s\n", js_buffer);
    return 0;
  }
  return result;
}

int main(int argc, const char *argv[]) {
  JSContextGroupRef contextGroup = JSContextGroupCreate();
  JSGlobalContextRef context =
      JSGlobalContextCreateInGroup(contextGroup, nullptr);
  JSObjectRef global_object = JSContextGetGlobalObject(context);

  add_document_class(context);

  std::ifstream file(argv[argc - 1]);
  std::stringstream stringstream;
  stringstream << file.rdbuf();
  file.close();

  JSStringRef script =
      JSStringCreateWithUTF8CString(stringstream.str().c_str());
  JSValueRef result = evaluate_script(context, script, global_object);

  JSGlobalContextRelease(context);
  JSContextGroupRelease(contextGroup);
}
